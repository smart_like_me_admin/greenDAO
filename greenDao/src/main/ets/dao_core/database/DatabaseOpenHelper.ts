/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Database } from './Database';
import { SQLiteOpenHelper } from './SQLiteOpenHelper'
import { StandardDatabase } from './StandardDatabase'
import { Migration } from '../dbflow/Migration'
import { StorageUtils } from '../StorageUtils'
import dataRdb from '@ohos.data.rdb'

/**
 * SQLiteOpenHelper to allow working with greenDAO's {@link Database} abstraction to create and update database schemas.
 */
export abstract class DatabaseOpenHelper extends SQLiteOpenHelper {
    private contexts: any
    private name: string
    private versions: number
    protected entitys: any
    private migrations: Array<Migration>

    constructor(context: any, name: string, version: number, factory?: any, errorHandler?: any) {
        super(context, name, version, factory, errorHandler);
        this.contexts = context;
        this.name = name;
        this.versions = version;
    }

    setEntitys(...entitys: any[]) {
        this.entitys = entitys;
        //entitys全局记录
        globalThis.entityCls = entitys;
    }

    setMigration(...migration: Array<Migration>) {
        this.migrations = migration
    }

    getMigration(): Array<Migration>{
        return this.migrations;
    }

    async setVersion(version: number) {
        this.versions = await StorageUtils.getValueByKey("dbVersion", 1)
        let db = await this.getWritableDb();
        if (version != this.versions) {
            if (version > this.versions) {
                this.onUpgrade_D(db, version, this.versions)
            } else if (version < this.versions) {
                throw Error("The database version does not support rollback")
            }
            this.versions = version;
            StorageUtils.putValue("dbVersion", this.versions);
        }
    }


    /**
     * Like {@link #getWritableDatabase()}, but returns a greenDAO abstraction of the database.
     * The backing DB is an standard {@link SQLiteDatabase}.
     */
    async getWritableDb(): Promise<Database> {
        return this.wrap(await this.getWritableDatabase());
    }

    /**
     * Like {@link #getReadableDatabase()}, but returns a greenDAO abstraction of the database.
     * The backing DB is an standard {@link SQLiteDatabase}.
     */
    async getReadableDb(): Promise<Database> {
        return this.wrap(await this.getReadableDatabase());
    }

    wrap(sqLiteDatabase: dataRdb.RdbStore): Database{
        return new StandardDatabase(sqLiteDatabase);
    }

    /**
     * Delegates to {@link #onCreate(Database)}, which uses greenDAO's database abstraction.
     */

    onCreate(db: dataRdb.RdbStore) {
        this.onCreate_D(this.wrap(db));
    }

    /**
     * Override this if you do not want to depend on {@link SQLiteDatabase}.
     */
    onCreate_D(db: Database) {
        //         Do nothing by default
    }

    /**
     * Delegates to {@link #onUpgrade(Database, int, int)}, which uses greenDAO's database abstraction.
     */
    onUpgrade(db: dataRdb.RdbStore, oldVersion: number, newVersion: number) {
        this.onUpgrade_D(this.wrap(db), oldVersion, newVersion);
    }

    /**
     * Override this if you do not want to depend on {@link SQLiteDatabase}.
     */
    onUpgrade_D(db: Database, oldVersion: number, newVersion: number) {
        // Do nothing by default
    }

    /**
     * Delegates to {@link #onOpen(Database)}, which uses greenDAO's database abstraction.
     */
    onOpen(db: dataRdb.RdbStore) {
        this.onOpen_D(this.wrap(db));
    }

    /**
     * Override this if you do not want to depend on {@link SQLiteDatabase}.
     */
    onOpen_D(db: Database) {
        // Do nothing by default
    }
}
