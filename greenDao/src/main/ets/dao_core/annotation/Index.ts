/*
  * Copyright (c) 2022 Huawei Device Co., Ltd.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
    *
  * http://www.apache.org/licenses/LICENSE-2.0
    *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

/**
 * Can be used to:
 * - specifies that the property should be indexed
 * - define multi-column index through {@link Entity#indexes()}
 *
 * @see Entity#indexes()
 */
class value {
    public readonly key: string = 'value';
    public value: any = '';

    constructor(value?: string) {
        if (value)
        this.value = value;
    }
}

class name {
    public readonly key: string = 'name';
    public value = '';

    constructor(value?: string) {
        if (value)
        this.value = value;
    }
}

class uniques {
    public readonly key: string = 'unique';
    public value = false;

    constructor(value?: boolean) {
        this.value = value;
    }
}

export class IndexEntity {
    public valueParam: value;
    public nameParam: name;
    public uniquesParam: uniques;

    constructor(valueStr: string = '', nameStr: string = '', uniqueBoolean: boolean = false) {
        this.valueParam = new value(valueStr);
        this.nameParam = new name(nameStr);
        this.uniquesParam = new uniques(uniqueBoolean);
    }
}


export function Index(params: IndexEntity = new IndexEntity()) {

    let valueTmp: value = params.valueParam;
    let nameTmp: name = params.nameParam;
    let uniqueTmp: uniques = params.uniquesParam;

    return (target, propertyKey) => {
        Reflect.defineMetadata(valueTmp.key, valueTmp.value, target, propertyKey); //'value'
        Reflect.defineMetadata(nameTmp.key, nameTmp.value, target, propertyKey); //'name'
        Reflect.defineMetadata(uniqueTmp.key, uniqueTmp.value, target, propertyKey); //'unique'
    }
}